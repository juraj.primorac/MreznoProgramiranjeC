#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>


typedef struct ripHeader{
  uint8_t command;
  uint8_t version;
  uint16_t mustBeZero;
} ripHeader;

typedef struct ripEntry{
  uint16_t afi;
  uint16_t rt;
  uint32_t ip;
  uint32_t subnetMask;
  uint32_t nextHop;
  uint32_t metric;
} ripEntry;


int main(int argc, char *argv[]) {
	
	int rFlag,wFlag,option;
	char *ip_usmjeritelja;
	
	rFlag=0;
	wFlag=0;

	
	if (argc<3) {
		errx(1, "Greska u pozivu naredbe\n");
	}
	
	while ((option=getopt(argc, argv, "r:w:"))!=-1) {
		
			switch(option) {
				case 'r': 	
							rFlag=1;
							ip_usmjeritelja=optarg;
							break;
				case 'w': 								
							wFlag=1;
							ip_usmjeritelja=optarg;
							break;
				default: 
							errx(1,"Greska u pozivu naredbe\n");
							break;
				
			}		
	
	}	
	/*
	printf("r=%d, w=%d\n", rFlag,wFlag);
	printf("ip_usmjeritelja:%s", ip_usmjeritelja);
	*/
	if (rFlag) {
		int mysock;	
		socklen_t toLen;
		struct sockaddr_in to;

		
		
		void *ripPointer;
		ripHeader *headerPointer;
		ripEntry *entryPointer;
		
		ripPointer=(void*)malloc(sizeof(ripHeader)+sizeof(ripEntry));
		headerPointer=(ripHeader*)ripPointer;
		entryPointer=(ripEntry*)(void*)(ripPointer+sizeof(ripHeader));
		
		memset(headerPointer,0,sizeof(ripHeader));
		memset(entryPointer,0,sizeof(ripEntry));
		
		headerPointer->command=0x01;
		headerPointer->version=0x02;

		entryPointer->metric=0x10000000;
		
		
		
		
		
		//(IPv4,datagram socket, default datagram protocol:udp)
		mysock = socket(PF_INET, SOCK_DGRAM,0);
		if (mysock<0) {
				errx(1, "socket greska\n");
			}

		memset(&to, 0, sizeof(to));
		to.sin_family = AF_INET;
		to.sin_port = htons(520);
		//samo za ipv4
		//prebaci u oblik za mrezu
		
		if (inet_aton(ip_usmjeritelja, &to.sin_addr) == 0) {
			errx(1, "inet_aton greska\n");
		}
		/*more ovako isto:
		 -destinacija je u network byte orderu
		 if (inet_pton(AF_INET, ip, &(to.sin_addr)) != 1) {
							errx(1,"%s nije valjana IP adresa", ip);
			}
		 */
		toLen=sizeof(to);
		//(udp socket,adresa podataka za slanje,duzine ove,flag je 0,sockaddr pokazivac strukture sa adresom odredista,sizeof)
		//vraca broj poslanih okteta
		if (sendto(mysock,ripPointer,sizeof(ripHeader)+sizeof(ripEntry),0,(struct sockaddr *) &to,toLen)==-1) {
			errx(1, "sendto greska\n");
		}
		
		int nbytes;
		nbytes=sizeof(ripHeader)+25*sizeof(ripEntry);
		void *ripPointerRecv=(void*)malloc(nbytes);
		int bRecv;
		//sve isto ali popuni socaddr sa adresom posiljatelja i toLen sa duzinom, vraca broj primljenih okteta pohranjenih u buffer
		//ceka!
		bRecv=recvfrom(mysock,ripPointerRecv,nbytes,0,(struct sockaddr *) &to,&toLen);
		if (bRecv==0 || bRecv==-1) {
			errx(1, "recvfrom greska\n");
		}
		
		
		
		
		
		
		
		int nOfEntryes;
		nOfEntryes=(bRecv-sizeof(ripHeader))/sizeof(ripEntry);
		
		
		ripHeader *headerPointerRecv;
		ripEntry *entryPointerRecv;
		headerPointerRecv=(ripHeader*)ripPointerRecv;
		entryPointerRecv=(ripEntry*)(ripPointerRecv+sizeof(ripHeader));
		
		uint32_t ipRecv;
		uint32_t subnetMaskRecv;
		uint32_t metricRecv;
		int i;		
		for (i=0;i<nOfEntryes;++i) {
			void *p= ripPointerRecv + sizeof(ripHeader)+ i*sizeof(ripEntry);
			ripEntry *e= (ripEntry *) p;
			
			//prebaci u host byte order, tj little-endian
			ipRecv=ntohl(e->ip); //dzabe sam ovo
			subnetMaskRecv=ntohl(e->subnetMask);//i ovo
			metricRecv=ntohl(e->metric);
			
			//ipRecv=(entryPointerRecv+i)->ip;
			//subnetMaskRecv=(entryPointerRecv+i)->subnetMask;	
			//metricRecv=(entryPointerRecv+i)->metric;
			
			
			//prebaci adrese u tockasti oblik
			char ipString[INET_ADDRSTRLEN],subnetMaskString[INET_ADDRSTRLEN];
			struct sockaddr_in sa;
			//sa.sin_addr.s_addr=ipRecv;
			sa.sin_addr.s_addr=e->ip;
			inet_ntop(AF_INET,&(sa.sin_addr),ipString,INET_ADDRSTRLEN);
			//sa.sin_addr.s_addr=subnetMaskRecv;
			sa.sin_addr.s_addr=e->subnetMask;
			inet_ntop(AF_INET,&(sa.sin_addr),subnetMaskString,INET_ADDRSTRLEN);
			
			
			printf("%s/%s metrika: %d\n", ipString,subnetMaskString,metricRecv);
		}
		
		
		close(mysock);
		
	}
	//napraviti i treci slucaj!.....
	if (wFlag) {
		
		
		int mysock;	
		int toLen;
		struct sockaddr_in to,myaddr;
		
		
		void *ripPointer;
		ripHeader *headerPointer;
		ripEntry *entryPointer;
		
		ripPointer=(void*)malloc(sizeof(ripHeader)+sizeof(ripEntry));
		headerPointer=(ripHeader*)ripPointer;
		entryPointer=(ripEntry*)(ripPointer+sizeof(ripHeader));
		
		headerPointer->command=0x02;
		headerPointer->version=0x02;
		headerPointer->mustBeZero=0x0000;
		
		entryPointer->afi=0x0200;
		entryPointer->rt=0x0000;
		entryPointer->ip=0x000d0c0b;
		entryPointer->subnetMask=0x00ffffff;
		entryPointer->nextHop=0x00000000;
		entryPointer->metric=0x01000000;
		
		
		toLen=sizeof(to);
		mysock = socket(AF_INET, SOCK_DGRAM, 0);
		if (mysock<0) {
				errx(1, "socket greska\n");
			}
		myaddr.sin_family=AF_INET;
		myaddr.sin_port=htons(520);
		myaddr.sin_addr.s_addr=htonl(INADDR_ANY);
		if (bind(mysock,(struct sockaddr *)&myaddr, sizeof(myaddr))==-1) {
			errx(1, "bind greska\n");
		}
		
			
			

		memset(&to, 0, sizeof(to));
		to.sin_family = AF_INET;
		to.sin_port = htons(520);
		if (inet_aton(ip_usmjeritelja, &to.sin_addr) == 0) {
			errx(1, "inet_aton greska\n");
		}
		

		do {
			if (sendto(mysock,ripPointer,sizeof(ripHeader)+sizeof(ripEntry),0,(struct sockaddr *) &to,toLen)==-1) {
				errx(1, "sendto greska\n");
			}
			sleep(5);
		} while(1);
		

	}
	return 0;
	
}
