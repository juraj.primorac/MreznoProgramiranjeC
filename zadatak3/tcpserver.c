#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <unistd.h>


#define BACKLOG 10
#define MAXLEN 10000


int main(int argc, char *argv[]) {
	
	
	int listenSocket, connSocket;
	struct sockaddr_in server_addr, client_addr;
	char recvBuffer[MAXLEN], sendBuffer[MAXLEN];
	
	
	
	
	int port=1234;
	int pFlag=0;
	char option;
	
	while((option=getopt(argc,argv,"p:")) !=-1) {
		switch (option) {
			case 'p':
				for (int i=0;i<strlen(optarg);++i) {
					if (!(isdigit(optarg[i]))) {
						errx(1,"Port is not a number!\n");
					}
				}
				port=atoi(optarg);
				pFlag=1;
				break;
			default:
				errx(1,"Usage: ./tcpserver [-p port]\n");
		}
	}
	
	
	if (argc != 2*pFlag +1) {
		errx(1,"Usage: ./tcpserver [-p port]\n");
	}
	
	listenSocket=socket(AF_INET,SOCK_STREAM,0);
	
	memset(&server_addr,'0',sizeof(server_addr));
	
	server_addr.sin_family=AF_INET;
	server_addr.sin_port=htons(port);
	server_addr.sin_addr.s_addr=htonl(INADDR_ANY);
	
	if (bind(listenSocket, (struct sockaddr *)&server_addr, sizeof(server_addr))==-1) {
		errx(1,"bind error\n");
	}
	
	if (listen(listenSocket,BACKLOG)==-1) {
		errx(1,"listen error\n");
		}
		
	while (1) {//    
		socklen_t s=sizeof(client_addr);                    
		connSocket=accept(listenSocket,(struct sockaddr*)&client_addr,&s);
		if (connSocket==-1) {
			errx(1,"accept error\n");
		}
		
		int error;
		char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
		if ((error=getnameinfo((struct sockaddr *)&client_addr, s,
                hbuf, sizeof(hbuf), sbuf, sizeof(sbuf),
                NI_NOFQDN | NI_NUMERICSERV))) {
    	errx(1, "%s", gai_strerror(error));
		}

		printf("\nserver: got connection from %s:%s\n", hbuf,sbuf);
		

		
		void *pointer=malloc(MAXLEN);
		unsigned int *offset_pointer= (unsigned int *) pointer;
		char *filename_pointer=(char *)(pointer + sizeof(unsigned int));			
	
		
		int nRecv;
		//(fd,void *buffer,duljina buffera,0)
		//vraca broj byteova procitanih
		if ((nRecv=recv(connSocket,pointer,MAXLEN,0))==-1) {
			errx(1,"recv error\n");
			}
			
		/*	
		printf("Filename: %s\n", filename_pointer);
		printf("Offset: %u\n",*offset_pointer);
		*/
		
		char z='0';
		
		//vraca pointer na first occurence
		if (strchr(filename_pointer,'/')!= NULL) {
			z='1'; 
			}
		if (z=='0') {
			if (access(filename_pointer,F_OK)==-1) {
				z='2'; 
				}
			}
		if (z=='0') {
			if (access(filename_pointer,R_OK)==-1) {
				z='3'; 
				}
			}
			
		void *send_pointer=malloc(MAXLEN);
		char *status_pointer=(char *) send_pointer;
		char *message_pointer=(char *)(send_pointer+1);
		int first=1;
		
		switch (z) {
			case '1':
				*status_pointer=(char) 0x01;
				sprintf(message_pointer,"File not in current directory");
				//printf("File not in current directory\n");
				break;
			case '2':
				*status_pointer=(char) 0x01;
				sprintf(message_pointer,"File doesnt exist");
				//printf("%s\n",message_pointer);
				break;
			case '3':
				*status_pointer=(char) 0x02;
				sprintf(message_pointer,"No read permission");
				//printf("No read permission\n");
				break;
			default:
				*status_pointer=(char) 0x00;

				break;
			}
			
		int nWritten;
		
		if (z!='0')	{
			nWritten=write(connSocket,send_pointer,1+strlen(message_pointer)+1);
			if (nWritten<0) {
				errx(1,"write error\n");
				}
			}
		else {
			FILE *file=fopen(filename_pointer,"r");
			printf("Offset: %d\n",*offset_pointer);
			int nRead=0;
			fseek(file,*offset_pointer,SEEK_SET);
			while (1) {
				int pom;
				pom=fread(message_pointer+nRead,1,MAXLEN-first,file);
				nRead+=pom;
				if (pom==0) break;
			}
			
			if (send(connSocket,send_pointer,first+nRead,0)==-1)
						{
						errx(1,"send error\n");
						}
					
			//first=0;
					
				fclose(file);			
			}
		
		close(connSocket);
	}
	
	
	close(listenSocket);
	return 0;
}
