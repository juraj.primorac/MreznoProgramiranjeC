#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <unistd.h>


#define MAXLEN 10000

int main(int argc, char *argv[]) {
	//sizeof(size_t) == sizeof(unsigned integer)
	//sizeof(ssize_t) == sizeof(integer)
	
	int mySocket;
	struct sockaddr_in server_info;
	struct addrinfo hints, *res;
	
	
	int port=1234;
	char *ip=malloc(MAXLEN);
	strcpy(ip,"127.0.0.1");
	
	
	char option;
	int sFlag=0;
	int pFlag=0;
	int cFlag=0;
	
	while((option=getopt(argc,argv,"p:s:c:")) !=-1) {
		switch (option) {
			case 'p':
				for (int i=0;i<strlen(optarg);++i) {
					if (!(isdigit(optarg[i]))) {
						errx(1,"Port is not a number!\n");
					}
				}
				port=atoi(optarg);
				pFlag=1;
				break;
				
			case 's':
				ip=optarg;
				sFlag=1;
				break;
			case 'c':
				cFlag=1;
				break;
			default:
				errx(1,"Usage: ./tcpklijent [-s server] [-p port] [-c] filename\n");
		}
	}
	
	if (argc != 1+2*pFlag+2*sFlag+cFlag+1) {
		errx(1,"Usage: ./tcpklijent [-s server] [-p port] [-c] filename\n");
	}
	
	
	FILE *file;
	char *filename=argv[argc-1];
	unsigned int offset=0;
	int new=1;
	
	
	if (cFlag) {
		if (access(filename,F_OK)!=-1) {
			if (access(filename,W_OK)==-1) {
				errx(1,"Unable to write to file\n");
			}
			else {
				file=fopen(filename,"r+");
				fseek(file,0,SEEK_END);
				offset=ftell(file);
				new=0;
			}		
		}
	}
	else {
		if (access(filename,F_OK)!=-1) {
			errx(1,"File already exists\n");
			}
		}
	
	
	
	
	
	//(ipv4,stream socket, default stream protocol:TCP)
	
	if ((mySocket=socket(AF_INET,SOCK_STREAM,0))<0) {
		errx(1,"socket error\n");
		}
	
	memset(&server_info,'0',sizeof(server_info));
	
	server_info.sin_family=AF_INET;
	server_info.sin_port=htons(port);
	
	if (inet_pton(AF_INET,ip,&server_info.sin_addr)<=0) {
		errx(1,"inet_pton error\n");
		}
	
	/*
	memset(&hints,0,sizeof(hints));
	hints.ai_family=AF_INET;
	hints.ai_flags= AI_CANONNAME;
	
	if (getaddrinfo(ip,NULL,&hints,&res)){
		errx(1,"getaddrinfo error\n");
	}
	server_info.sin_addr=((struct sockaddr_in *)res->ai_addr)->sin_addr;
	 */
	
	//nije potrebno bindati jer ce os dodijeliti slobodni tcp port i lokalnu ip adresu
	//vrsi se 3 way handshake
	if (connect(mySocket,(struct sockaddr *)&server_info,sizeof(server_info))<0) {
		errx(1,"connect error\n");
		}
	
		
	void *pointer=malloc(MAXLEN);
	unsigned int *offset_pointer= (unsigned int *) pointer;
	*offset_pointer=offset;
	char *filename_pointer=(char *)(pointer + sizeof(unsigned int));	
	sprintf(filename_pointer,"%s",filename);
	
	printf("Offset: %d\n", *offset_pointer);
	
	//(fd,void *buffer,duljina poruke u bytes,0)
	//vraca broj poslanih okteta	
	if (send(mySocket,pointer,sizeof(unsigned int)+strlen(filename_pointer)+1,0)==-1)
		{
		errx(1,"send error\n");
		}
		
	int prvi=1;
	int n;
	char *recv_pointer=malloc(MAXLEN);
	char *status_pointer=recv_pointer;
	char *message_pointer=recv_pointer+1;
	
	while((n=read(mySocket,recv_pointer,MAXLEN))) {
		if (n<0) errx(1,"read error\n");
		
		if (prvi) {			
			if (*status_pointer != (char)0x00) {
				char c=*((char *) recv_pointer);
				errx((int)c,message_pointer);
				}
			if (new) {
				file=fopen(filename,"w+");
				}
			
			}
		
		
		for (int i=prvi;i<n;++i) {
				printf("%c",recv_pointer[i]);
				
				
				if (fprintf(file,"%c",recv_pointer[i])<0) {
					fclose(file);
					errx(1,"fprintf error\n");
					}
				
				}
		prvi=0;	
			
		}
	fclose(file);
	free(recv_pointer);
	
	close(mySocket);
	free(pointer);
	free(ip);
	
	return 0;
}
