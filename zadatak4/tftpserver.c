#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <err.h>
#include <syslog.h>

#define MAXLEN 1024

void sendError(int socket,struct sockaddr_in to,char *errmsg) {
	
	char sendMsg[512];
	
	//kod za error
	sendMsg[0] = 0;
	sendMsg[1] = 5;
	
	//kod za not defined
	sendMsg[2] = 0;
	sendMsg[3] = 0;
	
	sprintf(&sendMsg[4],"%s",errmsg);
	
	int index=4+strlen(errmsg)+1;
	sendMsg[index] = 0;
	
	if (sendto(socket, sendMsg,4+strlen(errmsg)+1+1, 0, (struct sockaddr *) &to, sizeof(to)) == -1) {
		perror("send");
		}

}



//tftp-trivial file transfer protocol
//nad udp-om
//citanje/pisanje datoteka s/na server
int main(int argc, char *argv[]) {
	
	int mySocket,port;
	struct sockaddr_in myaddr,to;
	struct addrinfo hints,*res;
	
	int dFlag=0;
	
	char option;
	while((option = getopt(argc,argv, "d")) != -1) {
		switch(option){
		  case 'd':
				dFlag=1;
			break;
			
		  default:
			errx(1,"Usage: [-d] port_name_or_number\n");
			return 1;
		}
	}
	
	if ( 1+ dFlag + 1 != argc) {
		errx(1,"Usage: [-d] port_name_or_number\n");
		return 1;
	}
	
	char *portString = argv[argc - 1];
	
	if ((mySocket = socket(PF_INET,SOCK_DGRAM,0)) == -1) {
		errx(1,"Socket error\n");
	}
	
	//get port in network byte order
	memset(&hints, 0, sizeof (hints));
	hints.ai_protocol = IPPROTO_UDP;
	hints.ai_family = AF_INET;
	
	if (getaddrinfo(NULL,portString, &hints, &res)) {
		errx(1,"getaddrinfo error\n");
		}
		
	port = ((struct sockaddr_in *) res->ai_addr)->sin_port;
	
	memset(&myaddr,'0',sizeof(myaddr));	
	
	myaddr.sin_port = port;
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = INADDR_ANY;
	
	
	if (bind(mySocket, (struct sockaddr *)&myaddr, sizeof(myaddr)) == -1) {
    errx(1,"bind error\n");    
	}
	
	if (dFlag) {
		daemon(0, 0);
		openlog("jp48769:MrePro tftpserver",LOG_PID,LOG_FTP);
	}
	
	while (1) {
		socklen_t toLen=sizeof(to);
		
		char *buffer=malloc(MAXLEN);
		int nRecv;
		nRecv = recvfrom(mySocket, buffer, MAXLEN, 0,(struct sockaddr *) &to, &toLen);
		if (nRecv < 0) {
			if (dFlag) {
				syslog(LOG_INFO, "recvfrom error\n");
				return 1;		
				}
			else {
				errx(1,"recvfrom error\n");
			}
		}
		
		
		int error;
		char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
		if ((error=getnameinfo((struct sockaddr *)&to, toLen,hbuf, sizeof(hbuf), sbuf, sizeof(sbuf),
                NI_NOFQDN | NI_NUMERICSERV))) {
    	errx(1, "%s", gai_strerror(error));
		}
		
		//rrq/wrq: 2okteta(1=r,2=2)|naziv datoteke|0|nacin prijenosa|0 
		//data:2okteta(3)|2okteta(broj bloka)|<=512 okteta podataka   ---ako je <512 onda kraj transfera
		//error:2 okteta(5)|2 okteta(kod pogreske)|poruka o gresci|0
		//kodovi za error: 0 not defined,1 file not found,2 access violation,3 disk full, 
		//4 illegal tftp operation,5 unknown port,6 file already exists,7 no such user
		if (!(*buffer==0 && *(buffer+1)== 1)) {
			char errmsg[506];
			sprintf(errmsg,"Read request expected (RRQ)\n");
			if (dFlag) syslog(LOG_INFO,"Read request expected (RRQ)\n"); 
			else printf("%s",errmsg);
			sendError(mySocket,to,errmsg);
			return 1;
		}
		
	
	
		int index = 2;
		while(buffer[index] != '\0') {
			++index;
		}
		
		char *datoteka=malloc(100);
		char *nacinPrijenosa=malloc(100);
		datoteka[0]=0;
		nacinPrijenosa[0]=0;
		
		strcpy(datoteka, buffer+2);
		strcpy(nacinPrijenosa, buffer + index + 1);
		
		//printf("%s\n",datoteka);
		//printf("%s\n",nacinPrijenosa);
		
		
		if (dFlag) syslog(LOG_INFO, "%s->%s\n", hbuf,datoteka); 
		else printf("%s->%s\n", hbuf, datoteka);
  
		if (fork() == 0) {
			//mySend(datoteka,nacinPrijenosa,myaddr,to,dFlag);
			//char errmsg[506];
			//sprintf(errmsg,"Dobro je sve\n");
			//sendError(mySocket,to,errmsg);
			int newsock;
			if ((newsock = socket(PF_INET, SOCK_DGRAM, 0)) == -1) {
				if (!dFlag) errx(1,"fork socket error\n");
				else {
					syslog(LOG_INFO, "fork socket error\n");
					exit(1);
					}
			}

			struct timeval timeout;  
			timeout.tv_usec = 0;    
			timeout.tv_sec = 1;
			
			//timeout za cekanje za recv i send
			if (setsockopt(newsock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,
						sizeof(timeout)) < 0) {
			if (!dFlag) errx(1,"setsockopt error\n");
			else {
				syslog(LOG_INFO, "setsockopt failed\n");
				exit(1);
				}	
			}	

			int size; 
			int last = 1;
			FILE *file = fopen(datoteka, "r");
			char buffer[512]; 

			if (file != NULL) {
				char msg[1000];
				while ((size = fread(buffer, 1, sizeof(buffer), file)) >= 0) {
					
					//moze i 0 podataka!
					//if (size == 0) break;
					int tooMuch=1;
					for(int i=0; i<4; ++i) {		
						memset(msg, '\0', sizeof(msg));
						msg[0] = 0;
						msg[1] = 3;
						msg[2] = last / 256;
						msg[3] = last % 256;
						if (size>0) strcat(msg + 4, buffer);

						if (sendto(newsock, msg, size+4, 0, (struct sockaddr *) &to, sizeof(to)) == -1) {
						perror("send");
						}
					
					
						char buf[150];
						socklen_t clientlen = sizeof(to);
						int n = recvfrom(newsock, buf, 150, 0,(struct sockaddr *) &to, &clientlen);

						if (n < 0) continue;
						if (buf[0] != 0 || buf[1] != 4) { 
							char errmsg[506];
							sprintf(errmsg,"Ocekivao ack!\n");
							if (dFlag) syslog(LOG_INFO,"Ocekivao ack!\n"); 
							else printf("%s",errmsg);
							sendError(newsock,to,errmsg); 
							}
							
						int ack = buf[2]*16 + buf[3];
						if (ack > last) {
							if (dFlag) syslog(LOG_INFO, "Potvrden paket koji nije poslan!\n"); 
							else printf("Potvrden paket koji nije poslan!\n");
							char errmsg[506];
							sprintf(errmsg,"Potvrden paket koji nije poslan!\n");
							sendError(newsock,to,errmsg);
						}
						if (ack < last) continue;
						if (ack == last) { 						
							tooMuch=0; 
							break;						
							}
					}
					if (tooMuch) {
						if (dFlag) syslog(LOG_INFO, "Previse retransmisija!\n"); 
						else printf("Previse retransmisija!\n");
						char errmsg[506];
						sprintf(errmsg,"Previse retransmisija!\n");
						sendError(newsock,to,errmsg);	
						exit(1);
						}
				
					
					last++;
				}
				fclose(file);
			} else {
				if (dFlag) syslog(LOG_INFO, "Nepostojeca datoteka!\n"); 
				else printf("Nepostojeca datoteka!\n");
				char errmsg[506];
				sprintf(errmsg,"Nepostojeca datoteka!\n");
				sendError(newsock,to,errmsg);	
				exit(1);

			}
			
			close(newsock);
			exit(0);
		}
	}
	
	close(mySocket); 
	return 0;
}
