# Network programming

Basic concepts of network and distributed programming. TCP and UDP socket interface. Client and server design. Network applications based on UDP and TCP. HTTP requests. Design and implementation using threading and concurrency. Broadcast and multicast applications. SCTP socket interface. Security issues. Practical examples of network applications in Unix environment using C. 


## Prerequisites

OS used is FreeBSD(32bit) with C compiler included, Virtul machine software(VMware or VirtualBox) if FreeBSD is not host OS.


## Running the tests

- cd directory_with_makefile_and_c <br />
- make program <br />
- ./program

## Author

* **Juraj Primorac** 




