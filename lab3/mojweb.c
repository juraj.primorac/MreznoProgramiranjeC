#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <err.h>
#include <syslog.h>
#include <dirent.h>
#include <sys/stat.h>
#include <ctype.h>

#define BACKLOG 5
#define MAXLEN 10000

//fetch http://127.0.0.1/mala.txt
//curl -X "DELETE" http://localhost/nesto.txt

void sendError(int sock,unsigned int kod) {
	char *message;
	switch (kod) {
		case 404:
			message = "HTTP/1.0 404 Not Found\r\nContent-Length: 20\r\nContent-Type:  text/html\r\n\r\nNepostojeca datoteka";
			break;
		case 405:
			message = "HTTP/1.0 405 Method Not Allowed\r\nContent-Length: 17\r\nContent-Type:  text/html\r\n\r\nNepodrzana metoda";
			break;
		case 505:
			message = "HTTP/1.0 505 Bad Request\r\nContent-Length: 23\r\nContent-Type:  text/html\r\n\r\nNepodrzana http verzija";
			break;		
		}
	printf("%s\n",message);
	send(sock, message, strlen(message), 0);
}


void obradaZahtjeva(int sock) {
	
	
	//recv
	char *pointer=malloc(MAXLEN);				
	memset(pointer,0,MAXLEN);
	
	int nRecv;
	if ((nRecv=recv(sock,pointer,MAXLEN,0))==-1) {
			errx(1,"recv error\n");
			}
		
	//sve	
	//printf("\n%s\n",pointer);
	
	//obradi prvi red zahtjeva
	int prviSpaceInd=0;
	int drugiSpaceInd=0;
	int crInd=0;
	int lfInd=0;
	for (int i=0;(i<nRecv) && (!lfInd);++i) {
		
		if (pointer[i]==' ') {
			if (prviSpaceInd) drugiSpaceInd=i;
			else prviSpaceInd=i;
			}
			
		if (pointer[i]=='\r') {
			crInd=i;
			}	
			
		if (pointer[i]=='\n') {
			lfInd=i;
			}		
		}
	
	
	//odvoji metodu, uri i verziju
	char *metoda=malloc(100);
	memset(metoda,0,100);
	char *uri=malloc(100);
	memset(uri,0,100);
	char *verzija=malloc(100);
	memset(verzija,0,100);
	
	
	strncpy(metoda,pointer,prviSpaceInd);
	strncpy(uri,pointer+prviSpaceInd+1,drugiSpaceInd-prviSpaceInd-1);
	strncpy(verzija,pointer+drugiSpaceInd+1,crInd-drugiSpaceInd-1);
	
	
	/*
	printf("%s\n", metoda);
	printf("%s\n", uri);
	printf("%s\n", verzija);
	*/
	
	//provjeri metodu
	if (strcmp(metoda,"GET")) {
		sendError(sock,405);
		return;
		}
		
		
	//provjeri verziju	
	if (strcmp(verzija, "HTTP/1.1") != 0 && strcmp(verzija, "HTTP/1.0")!=0) {
		sendError(sock,505);
		return;
	}
	
		
	//provjeri uri
	char *radniDirUzorak=malloc(2);
	sprintf(radniDirUzorak,"/");
	if ((strcmp(uri,radniDirUzorak)==0) || (strcmp(uri,"/index.html")==0)) {
			//printf("\nlista dat!\n");
			//posalji listu dat
			char *tijelo = (char *) malloc(10000);
			memset(tijelo, 0,10000);
			int tijeloLength=0;
		
			DIR *dir;
			struct dirent *ent;
			if ((dir=opendir("."))!=NULL) {
				while ((ent = readdir(dir)) != NULL) {
					struct stat path_stat;
					stat(ent->d_name,&path_stat);
					if S_ISREG(path_stat.st_mode) {//jeli regularan file?
						tijeloLength+= sprintf(tijelo+tijeloLength, "<a href=\"%s\">%s</a> (%jd)<br>\n", ent->d_name, ent->d_name, (intmax_t)path_stat.st_size);					
						}			
				}
				closedir(dir);
			}
			else {
				errx(1,"Cant open dir\n");			
				}
			char *odgovor = (char *) malloc(2*10000);
			memset(odgovor, 0,10000);
			int glavaLength = sprintf(odgovor, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\nContent-Type:  text/html\r\n\r\n", tijeloLength);
			
			sprintf(odgovor+glavaLength,"%s",tijelo);
			
			
			send(sock,odgovor, glavaLength + tijeloLength, 0);
			//printf("\n%s",odgovor);
			free(tijelo);
			free(odgovor);
	}
	else {
			//provjeri da li ima trazeni resurs u radnom dir
			if (access(uri+1,R_OK)==-1) {			
				sendError(sock,404);
				return;
				}
			else {
				
						//salji file
						FILE *f = fopen(uri+1, "r");
									
						if (f != NULL) {
									
								char *procitano = (char *) malloc(1000*10);
								memset(procitano,0, 1000*10);

								int procitanoLength = 0;
								int nRead;

								while ((nRead = fread(procitano+procitanoLength, 1, sizeof(procitano), f)) > 0) {
									procitanoLength +=nRead;
								}	
									 			
								char *point;
								point=strrchr(uri+1,'.');
								
								//printf("\n%s",point);
								
								
								char *ext=malloc(100);
								if (point!=NULL) {
										if (strcmp(point, ".jpg") == 0) {
											ext = "image/jpeg";
										} else if (strcmp(point, ".pdf") == 0) {
											ext = "application/pdf";
										}	else if (strcmp(point, ".txt") == 0) {
											ext = "text/plain";
										} else if (strcmp(point, ".gif") == 0) {
											ext = "image/gif";
										}							
									}
								else {						
									sprintf(ext,"text/html");
									}
								
								char *odgovor = (char *) malloc(1000*10*2);	
								
								int glavaLength = sprintf(odgovor, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\nContent-Type:  %s\r\n\r\n", procitanoLength,ext);	
								memcpy(odgovor+glavaLength,procitano,procitanoLength);

								send(sock,odgovor,glavaLength+procitanoLength, 0);	
								
								//free(procitano);
								//free(odgovor);
								fclose(f);
						} 
						else {
								sendError(sock,405);
								return;
							}						
				}
		}
			
	close(sock);
}

int main(int argc, char *argv[]) {
	
	int tcp_port;
	char *tcp_port_string;
	
	struct sockaddr_in myaddr,to;
	
	if (argc==1) {
		tcp_port=80;	//default	
		}
	else { 
		if (argc==2) {
			tcp_port_string=argv[argc-1];
			for (int i=0;i<strlen(tcp_port_string);++i) {
				if (!isdigit(tcp_port_string[i])) errx(1,"port is not a number\n");
				}
			tcp_port=atoi(tcp_port_string);
			}
		else {
			errx(1,"usage: ./mojweb [tcp_port]\n");
			}
		}
		
	int mysock;	
	if ((mysock = socket(AF_INET,SOCK_STREAM,0)) == -1) {
		errx(1,"socket eror\n");
		}
				
		
	int true = 1;
	if (setsockopt(mysock,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(true)) < 0)
		errx(1, "setsockopt error\n");

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(tcp_port);
	myaddr.sin_addr.s_addr = INADDR_ANY;
	memset(myaddr.sin_zero,'\0', sizeof(myaddr.sin_zero));
	
	if (bind(mysock, (struct sockaddr *)&myaddr, sizeof(myaddr))!=0) {
		errx(1,"bind error\n");
	}

	if (listen(mysock, BACKLOG) == -1) {
		err(1,"listen");
	}	
	
	
	while(1) {
		socklen_t len = sizeof(to);
		int toSocket;
		if ((toSocket = accept(mysock, (struct sockaddr *)&to, &len)) == -1) {
			errx(1,"accept error");
		}
		
		if (fork() == 0) {
			obradaZahtjeva(toSocket);
			close(toSocket);
			exit(0);
		}
	}
	close(mysock);
	return 0;
		
	
	
}
