#include <stdio.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>





int main(int argc, char *argv[]) {
	
	
	
	short tcpFlag, udpFlag, hexaFlag, networkByteFlag, hostByteFlag, ipv4Flag, ipv6Flag, rFlag;
	char *hostname,*servicename,*ip,*port, ip4_adr[INET_ADDRSTRLEN],ip6_adr[INET6_ADDRSTRLEN];
	struct addrinfo hints, *result;
	int err, option;
	char nazivRacunala[100], nazivUsluge[100];


	

	rFlag=0;
	tcpFlag=0;
	udpFlag=0;	
	hexaFlag=0;
	hostByteFlag=0;
	networkByteFlag=0;
	ipv4Flag=0;
	ipv6Flag=0;	
	

	if (argc<3) {
			errx(1, "prog [-r] [-t|-u] [-x] [-h|-n] [-4|-6] {hostname|IP_address} {servicename|port}\n");
		}		
		
	while ((option=getopt(argc, argv, "rtuxhn46"))!=-1) {
		
		switch(option) {
			case 'r': 	
						rFlag=1;
						break;
			case 't': 	
						tcpFlag=1;
						break;
			case 'u': 	
						udpFlag=1;
						break;
			case 'x': 	
						hexaFlag=1;
						break;
			case 'h': 	
						hostByteFlag=1;
						break;
			case 'n': 	
						networkByteFlag=1;
						break;
			case '4': 
						ipv4Flag=1;
						break;
			case '6': 
						ipv6Flag=1;
						break;
			default: 
						errx(2,"prog [-r] [-t|-u] [-x] [-h|-n] [-4|-6] {hostname|IP_address} {servicename|port}\n");
						break;
			
		}
	}	
	
	/*
	if (argc - optind != 2) {																			
		errx(1,"prog [-r] [-t|-u] [-x] [-h|-n] [-4|-6] {hostname|IP_address} {servicename|port}\n");
	}
	*/
	int bla=0;
	if (rFlag || tcpFlag || udpFlag || hexaFlag || hostByteFlag || networkByteFlag
	 || ipv4Flag || ipv6Flag) bla=1;
	if (argc!=1+bla+1+1) {
		errx(1,"prog [-r] [-t|-u] [-x] [-h|-n] [-4|-6] {hostname|IP_address} {servicename|port}\n");
		}
	
	
	if (rFlag==0) {//hostname i usluga argumenti
		
		hostname=argv[argc-2];
		servicename=argv[argc-1];
	
		memset(&hints, 0, sizeof(hints));
			
		if (udpFlag) hints.ai_protocol = IPPROTO_UDP;
		else hints.ai_protocol=IPPROTO_TCP;

		if (ipv6Flag) hints.ai_family = AF_INET6;
		else hints.ai_family = AF_INET;
		
		hints.ai_flags |= AI_CANONNAME;		
		
		//prima ip ili naziv, tako jee	
		err=getaddrinfo(hostname,servicename, &hints, &result);	
		if (err) errx(1, "%s", gai_strerror(err));	
		//result=result->ai_next;
		

		if (ipv6Flag) inet_ntop(result->ai_family, &((struct sockaddr_in6 *) result->ai_addr)->sin6_addr, ip6_adr, INET6_ADDRSTRLEN);
		else inet_ntop(result->ai_family, &((struct sockaddr_in *) result->ai_addr)->sin_addr, ip4_adr, INET_ADDRSTRLEN);
		
		if (hexaFlag) printf("%s (%s) %04x\n",(!(ipv6Flag)) ? ip4_adr : ip6_adr, result->ai_canonname, (!(networkByteFlag)) ? ntohs(((struct sockaddr_in *) result->ai_addr)->sin_port):((struct sockaddr_in *) result->ai_addr)->sin_port);	//hex vrijednost
		
		else printf("%s (%s) %d\n", (!(ipv6Flag)) ? ip4_adr : ip6_adr, result->ai_canonname, (!(networkByteFlag)) ? ntohs(((struct sockaddr_in *) result->ai_addr)->sin_port):((struct sockaddr_in *) result->ai_addr)->sin_port);	//dec vrijednost
		
		freeaddrinfo(result);
		}
	else {//ip i port argumenti
		
		/*kao rezultat se ispisuju zadana IP adresa, naziv racunala (CNAME) i naziv usluge ˇ
koja odgovara broju porta navedenog u pozivu programa (servicename )*/
		struct sockaddr_in sa;
		struct sockaddr_in6 sa6;

		ip=argv[argc-2];
		port=argv[argc-1];
		int portInt;
		sscanf(port, "%d", &portInt);
				
		if (!(ipv6Flag)) {
		
			sa.sin_family=AF_INET;
			//sa.sin_port= (hostByteFlag) ? ntohs(portInt) : portInt;
			//sa.sin_port=ntohs(portInt);
			sa.sin_port=htons(portInt);
			
			if (inet_pton(AF_INET, ip, &(sa.sin_addr)) != 1) {
							errx(1,"%s nije valjana IP adresa", ip);
			}
			
			int z=0;
			if (!(udpFlag)) z |= NI_NAMEREQD; 
			else z |= NI_DGRAM;
		
			err= getnameinfo((struct sockaddr *)&sa,
								sizeof(struct sockaddr_in),
								nazivRacunala, sizeof(nazivRacunala), nazivUsluge, sizeof(nazivUsluge),
								z);
			if (err) errx(1, "getnameinfo: %s", gai_strerror(err));     

		}
		else {
			sa6.sin6_family=AF_INET6;
			//sa6.sin6_port= (!(networkByteFlag)) ? ntohs(portInt) : portInt;
			//sa6.sin6_port=ntohs(portInt);
			sa6.sin6_port=htons(portInt);
			
			if (inet_pton(AF_INET6, ip, &(sa6.sin6_addr)) != 1) {
							errx(1,"%s nije valjana IP adresa", ip);
			}
			
			int z=0;
			if (!(udpFlag)) z |= NI_NAMEREQD; 
			else z |= NI_DGRAM;
		
			err= getnameinfo((struct sockaddr *)&sa6,
								sizeof(struct sockaddr_in6),
								nazivRacunala, sizeof(nazivRacunala), nazivUsluge, sizeof(nazivUsluge),
								z);
			if (err) errx(1, "getnameinfo: %s", gai_strerror(err));   
		}
			

		printf("%s (%s) %s\n", ip, nazivRacunala, nazivUsluge);

	}

	
	


	return 0;
	
	
}
