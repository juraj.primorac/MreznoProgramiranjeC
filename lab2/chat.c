#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <err.h>
#include <errno.h>
#include <syslog.h>
#define STDIN 0
#define BACKLOG 5
#define MAXLEN 1000


/*start listening*/
void listen_udp_tcp(int *tcp_socket, int tcp_port, int *udp_socket, int udp_port, fd_set *top_set) {	
	struct sockaddr_in myaddr;
		
	
	//tcp
	if ((*tcp_socket = socket(AF_INET,SOCK_STREAM,0))==-1) {
		errx(1,"tcp socket error\n");
	}
	
	int true=1;
	if (setsockopt(*tcp_socket,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(true)) == -1) {
		errx(1,"tcp setsockopt error\n");
		}
	
	myaddr.sin_port=htons(tcp_port);
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = INADDR_ANY;
	memset(myaddr.sin_zero,'\0', sizeof(myaddr.sin_zero));
	if ( (bind(*tcp_socket, (struct sockaddr *)&myaddr, sizeof(myaddr)) )!=0) {
		errx(1,"tcp bind error\n");
		}
	
	if ((listen(*tcp_socket,BACKLOG))==-1) {
		errx(1,"listen error\n");
	}
	
	
	//udp
	if ((*udp_socket = socket(PF_INET,SOCK_DGRAM,0))==-1) {
		errx(1,"udp socket error\n");
	}
	
	if (setsockopt(*udp_socket,SOL_SOCKET,SO_REUSEADDR,&true,sizeof(true)) == -1) {
		errx(1,"udp setsockopt error\n");
		}
		
	myaddr.sin_port=htons(udp_port);
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = INADDR_ANY;
	memset(myaddr.sin_zero,'\0', sizeof(myaddr.sin_zero));
	if ( (bind(*udp_socket, (struct sockaddr *)&myaddr, sizeof(myaddr)) )!=0) {
		errx(1,"udp bind error\n");
		}
		
	//add to set of file descriptors
	FD_SET(*tcp_socket,top_set);
	FD_SET(*udp_socket,top_set);
	
}

int main(int argc, char *argv[]) {
	
	
	//this data types represent file descriptor sets
	fd_set top_set;
	fd_set select_set;
	int maxFdNum;
	//clear sets, zero bits for all file descriptors
	FD_ZERO(&top_set);
	FD_ZERO(&select_set);
	
	struct sockaddr_in myaddr;
	struct sockaddr_in client_addr;
	
	int tcp_socket;
	int tcp_port=1234;
	int udp_socket;
	int udp_port=1234;
	int daemon_socket;
	int kontrolni_port;
	
	int started=0;
	char *lozinka;
	char daemon_start[MAXLEN];
	char daemon_off[MAXLEN];
	char daemon_quit[MAXLEN];
	
	int acceptSocket;
	
	int tFlag=0;
	int uFlag=0;
	int kFlag=0;
	
	
	
	int option;
	while ((option=getopt(argc, argv, "t:u:k:"))!=-1) {
		
			switch(option) {
				
				case 't': 	
							tFlag=1;
							tcp_port=atoi(optarg);
							break;
				case 'u': 								
							uFlag=1;
							udp_port=atoi(optarg);
							break;
				case 'k': 								
							kFlag=1;
							kontrolni_port=atoi(optarg);
							lozinka=argv[argc-1];
							break;
							
				default: 
							errx(1,"Command error.\n");
							break;
				
			}	
	}
	
	if (kFlag) {
			
			//detach from controlling terminal and run in the background as system daemon
			//int nochdir->0 means current working dir changes to root,otherwise its unchanged
			//int noclose->0 means stdin,stdout,stderr idu u /dev/null/, otherwise ne diraju se ti deskriptori
			daemon(0,0);
			
			//opens or reopens connection to Syslog
			//(prefix za svaku poruku default je argv[0],
			//insert pid of calling process in message,
			//kod za konekciju LOG_FTP je ftp daemon
			openlog("jp48769:MrePro chat", LOG_PID,LOG_FTP);
			
			if ((daemon_socket=socket(PF_INET,SOCK_DGRAM,0))==-1) {
				errx(1,"daemon socket error\n");
				}
				
			myaddr.sin_port=htons(kontrolni_port);
			myaddr.sin_family = AF_INET;
			myaddr.sin_addr.s_addr = INADDR_ANY;
			memset(myaddr.sin_zero,'\0', sizeof(myaddr.sin_zero));
			if ( (bind(daemon_socket, (struct sockaddr *)&myaddr, sizeof(myaddr)) )!=0) {
				errx(1,"daemon bind error\n");
				}
				
			//dodaj file descriptor
			FD_SET(daemon_socket,&top_set);
			maxFdNum=daemon_socket;	
			
			sprintf(daemon_start,"ON:%s\n", lozinka);
			sprintf(daemon_off,"OFF:%s\n", lozinka);
			sprintf(daemon_quit,"QUIT:%s\n", lozinka);
		}
	else {
			listen_udp_tcp(&tcp_socket,tcp_port,&udp_socket,udp_port, &top_set);	
			FD_SET(STDIN,&top_set);
			maxFdNum = (tcp_socket > udp_socket) ? tcp_socket : udp_socket;
		}
	
	
	while (1) {
		
		select_set=top_set;
		if (select(maxFdNum+1,&select_set,NULL,NULL,NULL)==-1) {
			errx(1,"select error.\n");
			}
		
		int newTcpConn=1;
		int udpMsg=2;
		int stdinMsg=3;
		int daemon=4;
		
		int something=0;
		
		for (int i=0;i<=maxFdNum;++i) {
			//vraca !0 ako je fd unutar seta
			if FD_ISSET(i,&select_set){ //lets see..
				
				if (i==tcp_socket) {
					something=newTcpConn;
					}
				if (i==udp_socket) {
					something=udpMsg;
					}
				if (i==STDIN && !kFlag) {
					something=stdinMsg;
					}
				if (i==daemon_socket) {
					something=daemon;
					}
				
				int addrlen;
				char inputBuffer[MAXLEN];
				char sendBuffer[MAXLEN];
				char buffer[MAXLEN];
				switch (something) {
					
					case 1://newTcpConn
						addrlen=sizeof(client_addr);
						if ((acceptSocket=accept(tcp_socket,(struct sockaddr *)&client_addr,(socklen_t *)&addrlen ))==-1) {
							perror("accept error\n");
							}
						else {
							FD_SET(acceptSocket,&top_set);
							maxFdNum=(maxFdNum>acceptSocket) ? maxFdNum:acceptSocket;
							
							char host[NI_MAXHOST];
							char service[NI_MAXSERV];
							if(getnameinfo((struct sockaddr *)&client_addr,addrlen,host,sizeof(host),
							service,sizeof(service),NI_NOFQDN | NI_NUMERICSERV)) //NI_NUMERICHOST
								{
								errx(1,"getnameinfo error\n");
								}
							//print
							if (!kFlag) {
								fprintf(stderr,"Spojio se: %s:%s\n",host,service);
								}
							else {
								//daemon print
								//ide u /var/log/xferlog
								syslog(LOG_INFO,"Spojio se: %s:%s\n",host,service);
								}
							}
						break;
					case 2://udpMsg
						addrlen=sizeof(client_addr);
						char *pointerRecv=(char*)malloc(MAXLEN);
						int bRecv;
						bRecv=recvfrom(i,pointerRecv,MAXLEN,0,(struct sockaddr *) &client_addr,(socklen_t *)&addrlen );
						if (bRecv>0) {
							char host[NI_MAXHOST];
							char service[NI_MAXSERV];
							if(getnameinfo((struct sockaddr *)&client_addr,addrlen,host,sizeof(host),
							service,sizeof(service),NI_NOFQDN | NI_NUMERICSERV)) 
								{
								errx(1,"getnameinfo error\n");
								}						
							//print
							if (!kFlag) {
								fprintf(stderr,"UDP: %s:%s\n",host,service);
							}
							else {
								//daemon print
								syslog(LOG_INFO,"UDP: %s:%s\n",host,service);
								}
							
							//send
							memset(sendBuffer,'\0',sizeof(sendBuffer));
							sprintf(sendBuffer,"UDP: %s:%s: %s\n",host,service,pointerRecv);
							for (int j=0;j<=maxFdNum;++j) {
								if (FD_ISSET(j,&top_set)) {
									if (j!=daemon_socket && j!=tcp_socket && j!=udp_socket && j!=STDIN && j!=i) {
										if (send(j,sendBuffer,strlen(sendBuffer),0)==-1) {
											perror("send error\n");
											}
										}
									}
								}
								
						}
						free(pointerRecv);
						break;
					case 3://stdinMsg
						memset(inputBuffer,'\0',sizeof(inputBuffer));
						memset(sendBuffer,'\0',sizeof(sendBuffer));
						scanf("%s", inputBuffer);
						//send
						sprintf(sendBuffer,"Server: %s\n",inputBuffer);
							for (int j=0;j<=maxFdNum;++j) {
								if (FD_ISSET(j,&top_set)) {
									if (j!=daemon_socket && j!=tcp_socket && j!=udp_socket && j!=STDIN) {
										if (send(j,sendBuffer,strlen(sendBuffer),0)==-1) {
											perror("send error\n");
											}
										}
									}
								}
						break;
						
					case 4: //kontrolni port
						addrlen=sizeof(client_addr);
						memset(buffer,'\0',sizeof(buffer));
						int b;
						b=recvfrom(i,buffer,MAXLEN,0,(struct sockaddr *) &client_addr,(socklen_t *)&addrlen );
						if (b>0) {
							if (strcmp(buffer,daemon_start)==0 && started==0) {
								listen_udp_tcp(&tcp_socket,tcp_port,&udp_socket,udp_port, &top_set);	
								int veci = (tcp_socket > udp_socket) ? tcp_socket : udp_socket ;
								maxFdNum = (maxFdNum > veci) ? maxFdNum : veci;
								started=1;
								
								}
							if (strcmp(buffer,daemon_off)==0 && started==1) {
								started=0;
								close(udp_socket);
								close(tcp_socket);
								maxFdNum=daemon_socket;
								
								}
							if (strcmp(buffer,daemon_quit)==0) {
								started=0;
								close(udp_socket);
								close(tcp_socket);
								close(daemon_socket);
								closelog();
								return 0;
								}						
						}
						break;
					default://tcp message
						
						addrlen=sizeof(client_addr);
						memset(buffer,'\0',sizeof(buffer));
						getpeername(i,(struct sockaddr *)&client_addr,(socklen_t *)&addrlen);
						int n;
						n=recv(i,buffer,MAXLEN,0);
						if (n<0) {
							perror("recv error \n");
							}
						if (n==0) {//odspojio
							//print
							char host[NI_MAXHOST];
							char service[NI_MAXSERV];
							if(getnameinfo((struct sockaddr *)&client_addr,addrlen,host,sizeof(host),
							service,sizeof(service),NI_NOFQDN | NI_NUMERICSERV)) 
								{
								errx(1,"getnameinfo error\n");
								}
							if (!kFlag) {
								fprintf(stderr,"Odspojio se: %s:%s \n",host,service);
								}
							else {
								//daemon print
								syslog(LOG_INFO,"Odspojio se: %s:%s \n",host,service);
								}
							
							close(i);
							FD_CLR(i,&top_set);
							break;
							}
						else {
							char host[NI_MAXHOST];
							char service[NI_MAXSERV];
							if(getnameinfo((struct sockaddr *)&client_addr,addrlen,host,sizeof(host),
							service,sizeof(service),NI_NOFQDN | NI_NUMERICSERV)) 
								{
								errx(1,"getnameinfo error\n");
								}
							//print
							if (!kFlag) {
								fprintf(stderr,"TCP: %s:%s \n",host,service);
							}
							else {
								//daemon print
								syslog(LOG_INFO,"TCP: %s:%s \n",host,service);
								}
							//send
							sprintf(sendBuffer,"TCP: %s:%s: %s\n",host,service,buffer);
							for (int j=0;j<=maxFdNum;++j) {
								if (FD_ISSET(j,&top_set)) {
									if (j!=daemon_socket && j!=tcp_socket && j!=udp_socket && j!=STDIN && j!=i) {
										if (send(j,sendBuffer,strlen(sendBuffer),0)==-1) {
											perror("send error\n");
											}
										}
									}
								}
							}
							
						break;
								
					}
				
				
				}			
			}		
		}	
	
	return 0;
}
